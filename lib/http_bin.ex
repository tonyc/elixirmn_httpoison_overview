defmodule HttpBin do
  use HTTPoison.Base

  def process_url(url) do
    "https://httpbin.org/" <> url
  end

end

