defmodule IonoWrapper do
  use HTTPoison.Base

  def process_url(location) do
    "http://legacy-www.swpc.noaa.gov/ftpdir/lists/iono_day/" <> location <> "_iono.txt"
  end

  def process_response_body(body) do
    IO.puts "process_response_body"
    body
  end

  def process_request_headers(headers) do
    IO.puts "process_request_headers:"

    headers
  end

  def process_request_options(options) do
    IO.puts "process_request_options"

    options
  end

  # intercepts the response headers
  def process_headers(headers) do
    IO.puts "process_headers"

    headers
  end

  def process_status_code(status_code) do
    IO.puts "process_status_code"

    status_code
  end

end

