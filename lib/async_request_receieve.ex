defmodule AsyncRequestReceive do

  def run do
    HTTPoison.get "https://httpbin.org/stream-bytes/31337", %{}, stream_to: self()

    receive_loop()
  end

  defp receive_loop do
    receive do
      %HTTPoison.AsyncHeaders{headers: headers, id: _id} ->
        IO.puts "== Async Headers"
        IO.inspect headers
      %HTTPoison.AsyncStatus{code: code} ->
        IO.puts "== Async Status: #{code}"
      %HTTPoison.AsyncChunk{chunk: chunk, id: _id} ->
        IO.puts "== Async Chunk (#{byte_size(chunk)} bytes):"
        IO.inspect chunk
      %HTTPoison.AsyncEnd{id: _id} ->
        IO.puts "== Async END"
        exit :ok
    end

    receive_loop()
  end
end

