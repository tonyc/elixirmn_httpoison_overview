# HTTPoison basics

Basic API functions have two versions, with or without the bang:

    get/3
    put/4
    post/4
    delete/3
    options/3
    patch/4
    head/3

These all ultimately come down to request/5:

```elixir
request(method, url, body \\ "", headers \\ [], options \\ [])
# e.g.

{:ok, response} = HTTPoison.request(:get, "https://httpbin.org/status/200")

IO.puts HTTPoison.request!(:post, "https://httpbin.org/post", "POST Body", [{"X-Some-Header", "Foo"}]).body

```



## Without the bang returns tuple for pattern matching:

### Happy Path:
```elixir
{:ok, response} = HTTPoison.get "https://httpbin.org/uuid"
```

### Pattern matching:
```elixir
case HTTPoison.get(url, headers, options) do
  {:ok, response} ->
    IO.puts response.body
  {:error, %HTTPoison.Error{id: id, reason: reason}} ->
    IO.puts "Error!: #{reason}"
end
```

## With the bang, returns either an %HTTPoison.Response{} or raises %HTTPoison.Error{}:

    response = HTTPoison.get! "https://httpbin.org/uuid"
    IO.puts response.body

### In case something bad happens:

    response = HTTPoison.get! "https://sdkhsdiqwudisdADASD"

    ** (HTTPoison.Error) :nxdomain
        (httpoison) lib/httpoison.ex:66: HTTPoison.request!/5


    iex(1)> {:error, err} = HTTPoison.get "https://sdkhsdiqwudisdADASD"
    {:error, %HTTPoison.Error{id: nil, reason: :nxdomain}}

    iex(2)> err
    %HTTPoison.Error{id: nil, reason: :nxdomain}

# Wrapping HTTPoison.Base

Provide a simpler interface to a URL

```elixir
defmodule HttpBin do
  use HTTPoison.Base

  def process_url(url) do
    "https://httpbin.org/" <> url
  end
end

{:ok, response} = HttpBin.get("status/404")

%HTTPoison.Response{body: "",
 headers: [{"Connection", "keep-alive"}, {"Server", "meinheld/0.6.1"},
  {"Date", "Fri, 17 Nov 2017 13:45:19 GMT"},
  {"Content-Type", "text/html; charset=utf-8"},
  {"Access-Control-Allow-Origin", "*"},
  {"Access-Control-Allow-Credentials", "true"}, {"X-Powered-By", "Flask"},
  {"X-Processed-Time", "0.000690937042236"}, {"Content-Length", "0"},
  {"Via", "1.1 vegur"}], request_url: "https://httpbin.org/status/404",
 status_code: 404}


HttpBin.get("status/404").status_code
```

# Async

* Use stream_to: <pid> to have Hackney send messages to the supplied pid

```elixir
# Run this in iex:
HTTPoison.get "https://httpbin.org/stream-bytes/20480", %{}, stream_to: self()
flush()
```

## So how do we make use of this?
### -> Open async_request_receive.ex


